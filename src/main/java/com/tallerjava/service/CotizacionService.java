package com.tallerjava.service;

import com.tallerjava.modelo.Cotizacion;
import com.tallerjava.repository.BackupCotizacion;
import com.tallerjava.repository.CotizacionRepository;
import java.util.ArrayList;
import java.util.List;

public class CotizacionService {

    private List<CotizacionRepository> repositorios;
    private BackupCotizacion backupCotizacion = new BackupCotizacion();

    public CotizacionService(List<CotizacionRepository> repositorios) {
        this.repositorios = repositorios;
    }

    public List<Cotizacion> obtenerCotizaciones() {
        List<Cotizacion> cotizaciones = new ArrayList();
        Cotizacion cotizacion;
        for (CotizacionRepository repositorio : repositorios) {
            try {
                cotizacion = repositorio.obtenerCotizacion();
                cotizaciones.add(cotizacion);
                if (!"Finan Sur".equals(repositorio.getNombre())) {
                    backupCotizacion.guardarCotizacion(cotizacion);
                }
            } catch (Exception e) {
                try {
                    cotizaciones.add(backupCotizacion.recuperarCotizacion(repositorio.getNombre()));
                } catch (RuntimeException ex) {
                    cotizaciones.add(new Cotizacion(repositorio.getNombre(), null, null, 0.0f));
                }
            }
        }
        return cotizaciones;
    }

}
