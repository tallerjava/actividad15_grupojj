package com.tallerjava.presentacion;

import com.tallerjava.modelo.Cotizacion;
import com.tallerjava.repository.BinanceCotizacionRepository;
import com.tallerjava.repository.CoinDeskCotizacionRepository;
import com.tallerjava.repository.CotizacionRepository;
import com.tallerjava.repository.CryptoCotizacionRepository;
import com.tallerjava.repository.FinanSurCotizacionRepository;
import com.tallerjava.repository.SomosPNTCotizacionRepository;
import com.tallerjava.service.CotizacionService;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class App {

    public static void main(String[] args) throws IOException, ParseException {
        List<CotizacionRepository> repositorios = new ArrayList();
        repositorios.add(new CoinDeskCotizacionRepository());
        repositorios.add(new BinanceCotizacionRepository());
        repositorios.add(new SomosPNTCotizacionRepository());
        repositorios.add(new CryptoCotizacionRepository());
        repositorios.add(new FinanSurCotizacionRepository());

        try (Scanner sc = new Scanner(System.in)) {
            System.out.println("Presionar ENTER para ver la cotización y para salir culquier otra tecla y ENTER");
            String enterkey = sc.nextLine();
            SimpleDateFormat zonaHoraria = new SimpleDateFormat("d-M-yyyy hh:mm");
            CotizacionService service = new CotizacionService(repositorios);
            while (enterkey.isEmpty()) {
                System.out.println();
                List<Cotizacion> cotizaciones = service.obtenerCotizaciones();
                for (Cotizacion cotizacion : cotizaciones) {
                    String nombre = cotizacion.getProveedor() + ":";
                    if (cotizacion.getPrecio() != 0) {
                        System.out.format("%-15s %s / %s %6.2f %n", nombre, zonaHoraria.format(cotizacion.getFecha()), cotizacion.getMoneda(), cotizacion.getPrecio());
                    } else {
                        System.out.format("%-15s (el proveedor no se encuentra disponible) %n", nombre);
                    }
                }
                System.out.println("(Presione Enter para volver a consultar)");
                enterkey = sc.nextLine();
            }
        }
        System.out.println("Salió de la Cotización");
    }
}

