package com.tallerjava.repository;

import com.tallerjava.modelo.Cotizacion;
import java.util.Date;
import jodd.http.HttpRequest;
import jodd.http.HttpResponse;
import org.json.JSONObject;

public class BinanceCotizacionRepository extends CotizacionRepository {

    private String url = "https://api.binance.com/api/v1/ticker/price?symbol=BTCUSDT";
    private String nombre = "Binance";

    public String getUrl(String url) {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public String getNombre() {
        return nombre;
    }

    @Override
    public Cotizacion obtenerCotizacion() {
        Cotizacion cotizacion;
        HttpResponse response = HttpRequest.get(url).send();
        JSONObject namecampo = new JSONObject(response.body());
        double cotizacionBitcoin = namecampo.getDouble("price");
        String moneda = "USD";
        Date fecha = new Date();
        cotizacion = new Cotizacion(nombre, fecha, moneda, cotizacionBitcoin);
        return cotizacion;
    }

}
