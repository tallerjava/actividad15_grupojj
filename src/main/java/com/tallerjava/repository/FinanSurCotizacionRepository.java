package com.tallerjava.repository;

import com.tallerjava.modelo.Cotizacion;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class FinanSurCotizacionRepository extends CotizacionRepository {

    private String nombre = "Finan Sur";
    private String usuario = "Taller";
    private String password = "1234";

    @Override
    public String getNombre() {
        return nombre;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    @Override
    public Cotizacion obtenerCotizacion() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            try (Connection conexion = DriverManager.getConnection("jdbc:mysql://localhost:3306/finansur", usuario, password); Statement statement = conexion.createStatement()) {
                ResultSet result = statement.executeQuery("select * from cotizacion where fecha_cotizacion = (select max(fecha_cotizacion) from cotizacion);");
                if (result.next()) {
                    return new Cotizacion(nombre, result.getTimestamp(3), result.getString(4), result.getDouble(5));
                } else {
                    throw new RuntimeException("No hay cotizaciones");
                }
            }
        } catch (ClassNotFoundException | SQLException ex) {
            System.out.print(ex.getMessage());
            throw new RuntimeException(ex);
        }
    }
}
