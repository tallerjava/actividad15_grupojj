package com.tallerjava.repository;

import com.tallerjava.modelo.Cotizacion;
import java.util.Date;
import jodd.http.HttpRequest;
import jodd.http.HttpResponse;
import org.json.JSONObject;

public class CryptoCotizacionRepository extends CotizacionRepository {

    private String url = "https://min-api.cryptocompare.com/data/price?fsym=BTC&tsyms=USD";
    private String nombre = "Crypto Compare";

    public String getUrl(String url) {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public String getNombre() {
        return nombre;
    }

    @Override
    public Cotizacion obtenerCotizacion() {
        Cotizacion cotizacion;
        HttpResponse response = HttpRequest.get(url).send();
        JSONObject namecampo = new JSONObject(response.body());
        double cotizacionBitcoin = namecampo.getDouble("USD");
        String moneda = "USD";
        Date fecha = new Date();
        cotizacion = new Cotizacion(nombre, fecha, moneda, cotizacionBitcoin);
        return cotizacion;
    }
}
