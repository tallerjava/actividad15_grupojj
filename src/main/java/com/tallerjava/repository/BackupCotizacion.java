package com.tallerjava.repository;

import com.tallerjava.modelo.Cotizacion;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;

public class BackupCotizacion {

    private String usuario = "Taller";
    private String password = "1234";

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public Cotizacion recuperarCotizacion(String proveedor) {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            try (Connection conexion = DriverManager.getConnection("jdbc:mysql://localhost:3306/finansur", usuario, password); Statement statement = conexion.createStatement()) {
                ResultSet result = statement.executeQuery("select * from cotizacion order by fecha_cotizacion desc;");
                if (result.next()) {
                    return new Cotizacion(result.getString(2), result.getTimestamp(3), result.getString(4), result.getDouble(5));
                } else {
                    throw new RuntimeException("No hay cotizaciones");
                }
            }
        } catch (ClassNotFoundException | SQLException ex) {
            System.out.print(ex.getMessage());
            throw new RuntimeException(ex);
        }
    }

    public int guardarCotizacion(Cotizacion cotizacion) {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            SimpleDateFormat zonaHoraria = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            try (Connection conexion = DriverManager.getConnection("jdbc:mysql://localhost:3306/finansur", usuario, password); Statement statement = conexion.createStatement()) {
                statement.executeUpdate("INSERT INTO COTIZACION "
                        + "(proveedor, fecha_cotizacion, moneda, precio) values('"
                        + cotizacion.getProveedor() + "', '"
                        + zonaHoraria.format(cotizacion.getFecha()) + "', '"
                        + cotizacion.getMoneda() + "', '"
                        + cotizacion.getPrecio() + "');");
                statement.close();
            }
            return 1;
        } catch (ClassNotFoundException | SQLException ex) {
            return 0;
        }
    }
}
