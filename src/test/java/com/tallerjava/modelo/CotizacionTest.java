package com.tallerjava.modelo;

import java.util.Date;
import org.junit.Test;
import static org.junit.Assert.*;

public class CotizacionTest {

    @Test
    public void getProveedor_objetoCotizacionConValoresNormales_obtenerProveedor() {
        String resultadoEsperado = "CoinDesk";
        Date fecha = new Date(118, 8, 8);
        double precio = 12.123f;
        String moneda = "USD";
        Cotizacion instance = new Cotizacion(resultadoEsperado, fecha, moneda, precio);
        String resultadoObtenido = instance.getProveedor();
        assertEquals(resultadoEsperado, resultadoObtenido);
    }

    @Test
    public void getFecha_objetoCotizacionConValoresNormales_obtenerFecha() {
        String proveedor = "CoinDesk";
        Date resultadoEsperado = new Date(118, 8, 8);
        double precio = 12.123f;
        String moneda = "USD";
        Cotizacion instance = new Cotizacion(proveedor, resultadoEsperado, moneda, precio);
        Date resultadoObtenido = instance.getFecha();
        assertEquals(resultadoEsperado, resultadoObtenido);
    }

    @Test
    public void getPrecio_objetoCotizacionConValoresNormales_obtenerPrecio() {
        String proveedor = "CoinDesk";
        Date fecha = new Date(118, 8, 8);
        double resultadoEsperado = 12.123f;
        String moneda = "USD";
        Cotizacion instance = new Cotizacion(proveedor, fecha, moneda, resultadoEsperado);
        double resultadoObtenido = instance.getPrecio();
        assertEquals(resultadoEsperado, resultadoObtenido, 0.009);
    }

    @Test
    public void getMoneda_objetoCotizacionConValoresNormales_obtenerMoneda() {
        String proveedor = "CoinDesk";
        Date fecha = new Date(118, 8, 8);
        double precio = 12.123f;
        String resultadoEsperado = "USD";
        Cotizacion instance = new Cotizacion(proveedor, fecha, resultadoEsperado, precio);
        String resultadoObtenido = instance.getMoneda();
        assertEquals(resultadoEsperado, resultadoObtenido);
    }

    @Test
    public void setProveedor_objetoCotizacionConValoresNormales_cambiarProveedor() {
        String proveedor = "CoinDesk";
        Date fecha = new Date(118, 8, 8);
        double precio = 12.123f;
        String moneda = "USD";
        Cotizacion instance = new Cotizacion(proveedor, fecha, moneda, precio);
        String resultadoEsperado = "Somos PNT";
        instance.setProveedor(resultadoEsperado);
        String resultadoObtenido = instance.getProveedor();
        assertEquals(resultadoEsperado, resultadoObtenido);
    }

    @Test
    public void setFecha_objetoCotizacionConValoresNormales_cambiarFecha() {
        String proveedor = "CoinDesk";
        Date fecha = new Date(118, 8, 8);
        double precio = 12.123f;
        String moneda = "USD";
        Cotizacion instance = new Cotizacion(proveedor, fecha, moneda, precio);
        Date resultadoEsperado = new Date(118, 8, 9);
        instance.setFecha(resultadoEsperado);
        Date resultadoObtenido = instance.getFecha();
        assertEquals(resultadoEsperado, resultadoObtenido);
    }

    @Test
    public void setPrecio_objetoCotizacionConValoresNormales_cambiarPrecio() {
        String proveedor = "CoinDesk";
        Date fecha = new Date(118, 8, 8);
        double precio = 12.123f;
        String moneda = "USD";
        Cotizacion instance = new Cotizacion(proveedor, fecha, moneda, precio);
        double resultadoEsperado = 1554.1115;
        instance.setPrecio(resultadoEsperado);
        double resultadoObtenido = instance.getPrecio();
        assertEquals(resultadoEsperado, resultadoObtenido, 0.009);
    }

    @Test
    public void setMoneda_objetoCotizacionConValoresNormales_cambiarMoneda() {
        String proveedor = "CoinDesk";
        Date fecha = new Date(118, 8, 8);
        double precio = 12.123f;
        String moneda = "USD";
        Cotizacion instance = new Cotizacion(proveedor, fecha, moneda, precio);
        String resultadoEsperado = "Pesos";
        instance.setMoneda(resultadoEsperado);
        String resultadoObtenido = instance.getMoneda();
        assertEquals(resultadoEsperado, resultadoObtenido);
    }

}
