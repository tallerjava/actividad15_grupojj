package com.tallerjava.repository;

import com.tallerjava.modelo.Cotizacion;
import jodd.http.HttpException;
import org.junit.Test;
import static org.junit.Assert.*;

public class SomosPNTCotizacionRepositoryTest {

    @Test(expected = HttpException.class)
    public void obtenerCotizacion_urlIncorrecta_httpException() {
        SomosPNTCotizacionRepository instance = new SomosPNTCotizacionRepository();
        instance.setUrl("http://dev.somospn.com:9756/quote");
        Cotizacion resultadoObtenido = instance.obtenerCotizacion();
    }

    @Test(expected = HttpException.class)
    public void obtenerCotizacion_servidorNoResponde_jsonException() {
        SomosPNTCotizacionRepository instance = new SomosPNTCotizacionRepository();
        for (int i = 0; i < 20; i++) {
            Cotizacion resultadoObtenido = null;
            resultadoObtenido = instance.obtenerCotizacion();
            assertNotNull(resultadoObtenido);
        }
    }

}
