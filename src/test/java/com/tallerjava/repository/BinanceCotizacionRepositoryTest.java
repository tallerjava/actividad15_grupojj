package com.tallerjava.repository;

import com.tallerjava.modelo.Cotizacion;
import jodd.http.HttpException;
import org.junit.Test;
import static org.junit.Assert.*;

public class BinanceCotizacionRepositoryTest {

    @Test
    public void obtenerCotizacion_servicioResponde_objetoCotizacionNotNull() {
        BinanceCotizacionRepository instance = new BinanceCotizacionRepository();
        Cotizacion resultadoObtenido = instance.obtenerCotizacion();
        assertNotNull(resultadoObtenido);
    }

    @Test(expected = HttpException.class)
    public void obtenerCotizacion_urlIncorrecta_httpException() {
        BinanceCotizacionRepository instance = new BinanceCotizacionRepository();
        instance.setUrl("https://api.binanc.com/api/v1/ticker/price?symbol=BTCUSDT");
        Cotizacion resultadoObtenido = instance.obtenerCotizacion();
    }
}
