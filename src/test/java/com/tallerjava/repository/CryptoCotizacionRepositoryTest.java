package com.tallerjava.repository;

import com.tallerjava.modelo.Cotizacion;
import jodd.http.HttpException;
import org.junit.Test;
import static org.junit.Assert.*;

public class CryptoCotizacionRepositoryTest {

    @Test
    public void obtenerCotizacion_servicioResponde_objetoCotizacionNotNull() {
        CryptoCotizacionRepository instance = new CryptoCotizacionRepository();
        Cotizacion resultadoObtenido = instance.obtenerCotizacion();
        assertNotNull(resultadoObtenido);
    }

    @Test(expected = HttpException.class)
    public void obtenerCotizacion_urlIncorrecta_httpException() {
        CryptoCotizacionRepository instance = new CryptoCotizacionRepository();
        instance.setUrl("https://min-api.cryptocompar.com/data/price?fsym=BTC&tsyms=USD");
        Cotizacion resultadoObtenido = instance.obtenerCotizacion();
    }

}
