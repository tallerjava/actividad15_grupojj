package com.tallerjava.repository;

import com.tallerjava.modelo.Cotizacion;
import java.sql.SQLException;
import org.junit.Test;
import static org.junit.Assert.*;

public class FinanSurCotizacionRepositoryTest {

    @Test
    public void obtenerCotizacion_servicioResponde_objetoCotizacionNotNull() throws ClassNotFoundException, SQLException {
        FinanSurCotizacionRepository instance = new FinanSurCotizacionRepository();
        Cotizacion resultadoObtenido = null;
        resultadoObtenido = instance.obtenerCotizacion();
        assertNotNull(resultadoObtenido);
    }

    @Test(expected = RuntimeException.class)
    public void obtenerCotizacion_servicioNoResponde_SQLException() throws ClassNotFoundException, SQLException {
        FinanSurCotizacionRepository instance = new FinanSurCotizacionRepository();
        instance.setUsuario("Edeuterio");
        Cotizacion resultadoObtenido = null;
        resultadoObtenido = instance.obtenerCotizacion();
    }
}
