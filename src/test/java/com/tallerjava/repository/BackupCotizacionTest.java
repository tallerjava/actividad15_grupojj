package com.tallerjava.repository;

import com.tallerjava.modelo.Cotizacion;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.junit.Test;
import static org.junit.Assert.*;

public class BackupCotizacionTest {

    @Test
    public void testGuardarCotizacion() {
        CotizacionRepository repositorio = new CoinDeskCotizacionRepository();
        Cotizacion cotizacion = new Cotizacion(repositorio.getNombre(), new Date(), "USD", 7993.66f);
        BackupCotizacion backup = new BackupCotizacion();
        assertTrue(backup.guardarCotizacion(cotizacion) == 1);
    }

    @Test
    public void recuperarCotizacion_proveedorCoinDesk_retornaLaCotizacion() {

        CotizacionRepository repositorio = new CoinDeskCotizacionRepository();
        Cotizacion resultadoEsperado = new Cotizacion(repositorio.getNombre(), new Date(), "USD", 7993.66f);
        BackupCotizacion backup = new BackupCotizacion();
        backup.guardarCotizacion(resultadoEsperado);
        Cotizacion resultadoObtenido = backup.recuperarCotizacion(repositorio.getNombre());
        assertEquals(resultadoObtenido.getMoneda(), resultadoEsperado.getMoneda());
        assertEquals(resultadoObtenido.getProveedor(), resultadoEsperado.getProveedor());
        SimpleDateFormat zonaHoraria = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        assertEquals(zonaHoraria.format(resultadoObtenido.getFecha()), zonaHoraria.format(resultadoEsperado.getFecha()));
        assertEquals(resultadoObtenido.getPrecio(), resultadoEsperado.getPrecio(), 0.01);
    }

}
