mysql -u root

CREATE DATABASE finansur;

USE finansur;

GRANT ALL PRIVILEGES ON finansur.* TO 'Taller'@localhost IDENTIFIED BY '1234';

CREATE TABLE COTIZACION (
	cotizacion_id MEDIUMINT NOT NULL AUTO_INCREMENT,
	proveedor VARCHAR(20) NOT NULL,
	fecha_cotizacion DATETIME NOT NULL,
	moneda CHAR(3) NOT NULL,
	precio DOUBLE(16,2) DEFAULT '0.00' NOT NULL,
	UNIQUE(fecha_cotizacion),
	PRIMARY KEY (cotizacion_id)
);

INSERT INTO COTIZACION(proveedor, fecha_cotizacion, moneda, precio) values('Finan Sur', '2018-07-11 09:06:13', 'USD', '7403.57');
INSERT INTO COTIZACION(proveedor, fecha_cotizacion, moneda, precio) values('Finan Sur', '2018-07-11 10:05:12', 'USD', '7403.51');
INSERT INTO COTIZACION(proveedor, fecha_cotizacion, moneda, precio) values('Finan Sur', '2018-07-11 11:30:17', 'USD', '7403.52');
INSERT INTO COTIZACION(proveedor, fecha_cotizacion, moneda, precio) values('Finan Sur', '2018-07-11 12:13:40', 'USD', '7403.54');
INSERT INTO COTIZACION(proveedor, fecha_cotizacion, moneda, precio) values('Finan Sur', '2018-07-11 13:15:50', 'USD', '7403.27');
INSERT INTO COTIZACION(proveedor, fecha_cotizacion, moneda, precio) values('Finan Sur', '2018-07-11 14:03:16', 'USD', '7403.54');
INSERT INTO COTIZACION(proveedor, fecha_cotizacion, moneda, precio) values('Finan Sur', '2018-07-11 15:05:20', 'USD', '7403.55');
INSERT INTO COTIZACION(proveedor, fecha_cotizacion, moneda, precio) values('Finan Sur', '2018-07-11 16:00:34', 'USD', '7403.67');
INSERT INTO COTIZACION(proveedor, fecha_cotizacion, moneda, precio) values('Finan Sur', '2018-07-11 17:03:14', 'USD', '7403.56');
INSERT INTO COTIZACION(proveedor, fecha_cotizacion, moneda, precio) values('Finan Sur', '2018-07-11 18:01:04', 'USD', '7403.57');

INSERT INTO COTIZACION(proveedor, fecha_cotizacion, moneda, precio) values('Finan Sur', '2018-07-12 09:06:13', 'USD', '7400.57');
INSERT INTO COTIZACION(proveedor, fecha_cotizacion, moneda, precio) values('Finan Sur', '2018-07-12 10:05:12', 'USD', '7400.07');
INSERT INTO COTIZACION(proveedor, fecha_cotizacion, moneda, precio) values('Finan Sur', '2018-07-12 11:30:17', 'USD', '7400.17');
INSERT INTO COTIZACION(proveedor, fecha_cotizacion, moneda, precio) values('Finan Sur', '2018-07-12 12:13:40', 'USD', '7304.17');
INSERT INTO COTIZACION(proveedor, fecha_cotizacion, moneda, precio) values('Finan Sur', '2018-07-12 13:15:50', 'USD', '7304.17');
INSERT INTO COTIZACION(proveedor, fecha_cotizacion, moneda, precio) values('Finan Sur', '2018-07-12 14:03:16', 'USD', '7304.17');
INSERT INTO COTIZACION(proveedor, fecha_cotizacion, moneda, precio) values('Finan Sur', '2018-07-12 15:05:20', 'USD', '7304.27');
INSERT INTO COTIZACION(proveedor, fecha_cotizacion, moneda, precio) values('Finan Sur', '2018-07-12 16:00:34', 'USD', '7304.37');
INSERT INTO COTIZACION(proveedor, fecha_cotizacion, moneda, precio) values('Finan Sur', '2018-07-12 17:03:14', 'USD', '7304.47');
INSERT INTO COTIZACION(proveedor, fecha_cotizacion, moneda, precio) values('Finan Sur', '2018-07-12 18:01:04', 'USD', '7304.57');

INSERT INTO COTIZACION(proveedor, fecha_cotizacion, moneda, precio) values('Finan Sur', '2018-07-13 09:06:13', 'USD', '7324.57');
INSERT INTO COTIZACION(proveedor, fecha_cotizacion, moneda, precio) values('Finan Sur', '2018-07-13 10:05:12', 'USD', '7334.07');
INSERT INTO COTIZACION(proveedor, fecha_cotizacion, moneda, precio) values('Finan Sur', '2018-07-13 11:30:17', 'USD', '7344.17');
INSERT INTO COTIZACION(proveedor, fecha_cotizacion, moneda, precio) values('Finan Sur', '2018-07-13 12:13:40', 'USD', '7354.17');
INSERT INTO COTIZACION(proveedor, fecha_cotizacion, moneda, precio) values('Finan Sur', '2018-07-13 13:15:50', 'USD', '7303.17');
INSERT INTO COTIZACION(proveedor, fecha_cotizacion, moneda, precio) values('Finan Sur', '2018-07-13 14:03:16', 'USD', '7434.17');
INSERT INTO COTIZACION(proveedor, fecha_cotizacion, moneda, precio) values('Finan Sur', '2018-07-13 15:05:20', 'USD', '7324.27');
INSERT INTO COTIZACION(proveedor, fecha_cotizacion, moneda, precio) values('Finan Sur', '2018-07-13 16:00:34', 'USD', '7334.37');
INSERT INTO COTIZACION(proveedor, fecha_cotizacion, moneda, precio) values('Finan Sur', '2018-07-13 17:03:14', 'USD', '7334.47');
INSERT INTO COTIZACION(proveedor, fecha_cotizacion, moneda, precio) values('Finan Sur', '2018-07-13 18:01:04', 'USD', '7305.57');

INSERT INTO COTIZACION(proveedor, fecha_cotizacion, moneda, precio) values('Finan Sur', '2018-07-14 09:06:13', 'USD', '7128.57');
INSERT INTO COTIZACION(proveedor, fecha_cotizacion, moneda, precio) values('Finan Sur', '2018-07-14 10:05:12', 'USD', '7128.07');
INSERT INTO COTIZACION(proveedor, fecha_cotizacion, moneda, precio) values('Finan Sur', '2018-07-14 11:30:17', 'USD', '7128.17');
INSERT INTO COTIZACION(proveedor, fecha_cotizacion, moneda, precio) values('Finan Sur', '2018-07-14 12:13:40', 'USD', '7128.07');
INSERT INTO COTIZACION(proveedor, fecha_cotizacion, moneda, precio) values('Finan Sur', '2018-07-14 13:15:50', 'USD', '7128.07');
INSERT INTO COTIZACION(proveedor, fecha_cotizacion, moneda, precio) values('Finan Sur', '2018-07-14 14:03:16', 'USD', '7128.17');
INSERT INTO COTIZACION(proveedor, fecha_cotizacion, moneda, precio) values('Finan Sur', '2018-07-14 15:05:20', 'USD', '7128.27');
INSERT INTO COTIZACION(proveedor, fecha_cotizacion, moneda, precio) values('Finan Sur', '2018-07-14 16:00:34', 'USD', '7128.37');
INSERT INTO COTIZACION(proveedor, fecha_cotizacion, moneda, precio) values('Finan Sur', '2018-07-14 17:03:14', 'USD', '7128.47');
INSERT INTO COTIZACION(proveedor, fecha_cotizacion, moneda, precio) values('Finan Sur', '2018-07-14 18:01:04', 'USD', '7128.57');

INSERT INTO COTIZACION(proveedor, fecha_cotizacion, moneda, precio) values('Finan Sur', '2018-07-15 09:06:13', 'USD', '7528.57');
INSERT INTO COTIZACION(proveedor, fecha_cotizacion, moneda, precio) values('Finan Sur', '2018-07-15 10:05:12', 'USD', '7528.07');
INSERT INTO COTIZACION(proveedor, fecha_cotizacion, moneda, precio) values('Finan Sur', '2018-07-15 11:30:17', 'USD', '7528.17');
INSERT INTO COTIZACION(proveedor, fecha_cotizacion, moneda, precio) values('Finan Sur', '2018-07-15 12:13:40', 'USD', '7528.07');
INSERT INTO COTIZACION(proveedor, fecha_cotizacion, moneda, precio) values('Finan Sur', '2018-07-15 13:15:50', 'USD', '7528.07');
INSERT INTO COTIZACION(proveedor, fecha_cotizacion, moneda, precio) values('Finan Sur', '2018-07-15 14:03:16', 'USD', '7528.17');
INSERT INTO COTIZACION(proveedor, fecha_cotizacion, moneda, precio) values('Finan Sur', '2018-07-15 15:05:20', 'USD', '7528.27');
INSERT INTO COTIZACION(proveedor, fecha_cotizacion, moneda, precio) values('Finan Sur', '2018-07-15 16:00:34', 'USD', '7528.37');
INSERT INTO COTIZACION(proveedor, fecha_cotizacion, moneda, precio) values('Finan Sur', '2018-07-15 17:03:14', 'USD', '7528.47');
INSERT INTO COTIZACION(proveedor, fecha_cotizacion, moneda, precio) values('Finan Sur', '2018-07-15 18:01:04', 'USD', '7528.57');

